#!/usr/bin/python
# -*- coding: utf-8 -*-
 
# source d'inspiration: http://wiki.wxpython.org/cx_freeze
 
import sys, os
from cx_Freeze import setup, Executable
 
#############################################################################
# preparation des options 
path = sys.path
includes = [ "re", "atexit" ]
excludes = []
packages = ["sys", "PyQt4.QtGui","PyQt4.QtCore","socket","Interface"]
includefiles = ["config.ini"]


options = {"path": path,
           "includes": includes,
		   "include_files": includefiles,
           "excludes": excludes,
           "packages": packages
           }
 
#############################################################################
# preparation des cibles
base = None
if sys.platform == "win32":
    base = "Win32GUI"
 
cible_1 = Executable(
    script = "client.py",
    base = base,
    compress = True,
    icon = None,
    )
 
#############################################################################
# creation du setup
setup(
    name = "Client",
    version = "4",
    description = "PiBot Client",
    author = "speedi57",
    options = {"build_exe": options},
    executables = [cible_1]
    )