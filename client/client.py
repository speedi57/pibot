#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import sip
sip.setapi('QVariant', 2)
from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import QMessageBox
from Interface import Ui_MainWindow
import socket,configparser
# Le deuxième exemple montre l'approche par héritage
# unique ou l'on sous classe QMainWindow et configurons
# l'interface utilisateur dans la méthode init() :
class MonAppli(QtGui.QMainWindow):
    def message(self):
        try:
            self.connexion_avec_serveur = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.connexion_avec_serveur.connect((self.hote, self.port))
            self.ui.stackedWidget.setCurrentIndex(1)
        except Exception as e:
            QMessageBox.about(self, "Attention", "Impossible de se connecter :" + str(e))
            pass
        return
    def sendMessage(self,message):
        self.msg_a_envoyer = str(message)
        # Peut planter si vous tapez des caractères spéciaux
        self.msg_a_envoyer = self.msg_a_envoyer.encode()
        # On envoie le message
        self.connexion_avec_serveur.send(self.msg_a_envoyer)
    def Avance(self):
        self.sendMessage("avance")
        return
    def Droite(self):
        self.sendMessage("Droite")
        return
    def Gauche(self):
        self.sendMessage("Gauche")
        return
    def Recule(self):
        self.sendMessage("Recule")
        return
    def Quit(self):
        self.Stop()
        self.connexion_avec_serveur.close()
        sys.exit(0)
        return
    def Stop(self):
        self.sendMessage("Stop")
        return
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.config = configparser.ConfigParser()
        try:
            self.config.read('config.ini')
            self.hote = self.config['Network']['IP']
            self.port = int(self.config['Network']['port'])
        except Exception as e:
            QMessageBox.about(self, "Attention", "Probleme fichier config :" + str(e))
        # Configure l'interface utilisateur.
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        # Connecte le bouton.
        self.connect(self.ui.Connect, QtCore.SIGNAL("clicked()"),self.message)
        self.connect(self.ui.Avance, QtCore.SIGNAL("clicked()"),self.Avance)
        self.connect(self.ui.Droite, QtCore.SIGNAL("clicked()"),self.Droite)
        self.connect(self.ui.Gauche, QtCore.SIGNAL("clicked()"),self.Gauche)
        self.connect(self.ui.Recule, QtCore.SIGNAL("clicked()"),self.Recule)
        self.connect(self.ui.Disconnect, QtCore.SIGNAL("clicked()"),self.Quit)
        self.connect(self.ui.Stop, QtCore.SIGNAL("clicked()"),self.Stop)
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = MonAppli()

    window.show()
    sys.exit(app.exec_())