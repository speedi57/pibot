# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created: Thu Jan 15 21:45:26 2015
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(640, 480)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(640, 480))
        MainWindow.setMaximumSize(QtCore.QSize(640, 480))
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.stackedWidget = QtGui.QStackedWidget(self.centralwidget)
        self.stackedWidget.setGeometry(QtCore.QRect(0, 0, 631, 451))
        self.stackedWidget.setObjectName(_fromUtf8("stackedWidget"))
        self.page = QtGui.QWidget()
        self.page.setObjectName(_fromUtf8("page"))
        self.Connect = QtGui.QPushButton(self.page)
        self.Connect.setGeometry(QtCore.QRect(150, 140, 321, 151))
        self.Connect.setObjectName(_fromUtf8("Connect"))
        self.stackedWidget.addWidget(self.page)
        self.page_2 = QtGui.QWidget()
        self.page_2.setObjectName(_fromUtf8("page_2"))
        self.Disconnect = QtGui.QPushButton(self.page_2)
        self.Disconnect.setGeometry(QtCore.QRect(470, 360, 161, 81))
        self.Disconnect.setObjectName(_fromUtf8("Disconnect"))
        self.Stop = QtGui.QPushButton(self.page_2)
        self.Stop.setGeometry(QtCore.QRect(240, 190, 161, 71))
        self.Stop.setObjectName(_fromUtf8("Stop"))
        self.Droite = QtGui.QPushButton(self.page_2)
        self.Droite.setGeometry(QtCore.QRect(450, 180, 161, 81))
        self.Droite.setObjectName(_fromUtf8("Droite"))
        self.Avance = QtGui.QPushButton(self.page_2)
        self.Avance.setGeometry(QtCore.QRect(240, 90, 161, 81))
        self.Avance.setObjectName(_fromUtf8("Avance"))
        self.Gauche = QtGui.QPushButton(self.page_2)
        self.Gauche.setGeometry(QtCore.QRect(20, 190, 161, 81))
        self.Gauche.setObjectName(_fromUtf8("Gauche"))
        self.Recule = QtGui.QPushButton(self.page_2)
        self.Recule.setGeometry(QtCore.QRect(240, 290, 161, 81))
        self.Recule.setObjectName(_fromUtf8("Recule"))
        self.stackedWidget.addWidget(self.page_2)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 640, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        MainWindow.setMenuBar(self.menubar)

        self.retranslateUi(MainWindow)
        self.stackedWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Client", None))
        self.Connect.setText(_translate("MainWindow", "Connection", None))
        self.Disconnect.setText(_translate("MainWindow", "Disconnect", None))
        self.Stop.setText(_translate("MainWindow", "Stop", None))
        self.Droite.setText(_translate("MainWindow", "Droite", None))
        self.Avance.setText(_translate("MainWindow", "Avance", None))
        self.Gauche.setText(_translate("MainWindow", "Gauche", None))
        self.Recule.setText(_translate("MainWindow", "Recule", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

