class Tactile:
    """Draw PyRobot Interface"""
    def __init__(self,screen,pygame) :
        self.screen = screen 
        self.pygame = pygame
        #define colours
        self.blue = 26, 0, 255
        self.cream = 254, 255, 250
        self.black = 0, 0, 0
        self.white = 255, 255, 255
    
    def view(self):
        """ Draw the global interface"""
        self.screen.fill(self.blue) #change the colours if needed
        self.pygame.draw.rect(self.screen, self.white, (0,0,320,240),1)
        #ecart = 50
        self.make_button("Menu item 1", 20, 20)
        self.make_button("Menu item 2", 20, 70)
        self.make_button("Menu item 3", 20, 120)
        self.make_button("Quit", 20, 170)
    
    def make_button(self,text, xpo, ypo, colour = (255, 255, 255)):
        """ Draw Botton with Text"""
        font=self.pygame.font.Font(None,24)
        label=font.render(str(text), 1, (colour))
        self.screen.blit(label,(xpo,ypo))
        self.pygame.draw.rect(self.screen, self.cream, (xpo-5,ypo-5,110,35),1)
    
    def SetText(self,text, xpo, ypo):
        """Write texte"""
        font=self.pygame.font.Font(None,24)
        label=font.render(str(text), 1, (self.white))
        self.pygame.draw.rect(self.screen, self.blue, (xpo-5,ypo-5,130,35),0)
        self.screen.blit(label,(xpo,ypo))


    def on_click(self,pos):
        """Search if button was clicked"""
        click_pos = pos
        #now check to see if button 1 was pressed
        if 15 <= click_pos[0] <= 125 and 15 <= click_pos[1] <=50:
            print "You pressed button 1"
    #now check to see if button 2 was pressed
        if 15 <= click_pos[0] <= 125 and 65 <= click_pos[1] <=100:
            print "You pressed button 2"
        #now check to see if button 3 was pressed
        if 15 <= click_pos[0] <= 125 and 115 <= click_pos[1] <=150:
            print "You pressed button 3"
        #now check to see if button 4 was pressed
        if 15 <= click_pos[0] <= 125 and 165 <= click_pos[1] <=200:
            return -1
        else :
            return None


