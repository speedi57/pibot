# -*- coding:utf-8 -*-
from threading import Thread
import socket
import select
class Com(Thread):
    """Socket Serveur for external pilotage"""
    def __init__(self):
        """ set setting of server """
        Thread.__init__(self)
        self.hote = ''
        self.port = 12800
        self.connect = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.exit = True
        self.cmd = None
    def run(self):
        """main threading loop"""
        self.connect.bind((self.hote, self.port))
        self.connect.listen(5)
        self.clients_connectes = []
        while self.exit :
               # On va verifier que de nouveaux clients ne demandent pas a se connecter
                # Pour cela, on ecoute la connexion_principale en lecture
                # On attend maximum 50ms
                connexions_demandees, wlist, xlist = select.select([self.connect],
                    [], [], 0.05)
    
                for connexion in connexions_demandees:
                    connexion_avec_client, infos_connexion = connexion.accept()
                    # On ajoute le socket connecte a la liste des clients
                    self.clients_connectes.append(connexion_avec_client)
    
                # Maintenant, on ecoute la liste des clients connectes

                # Les clients renvoyes par select sont ceux devant �tre lus (recv)
                # On attend la encore 50ms maximum
                # On enferme l'appel a select.select dans un bloc try
                # En effet, si la liste de clients connectes est vide, une exception
                # Peut �tre levee
                clients_a_lire = []
                try:
                    clients_a_lire, wlist, xlist = select.select(self.clients_connectes,
                            [], [], 0.05)
                except select.error:
                    pass
                else:
                    # On parcourt la liste des clients a lire
                    for client in clients_a_lire:
                        try :
                            # Client est de type socket
                            self.cmd = client.recv(1024)
                            # Peut planter si le message contient des caracteres speciaux
                            self.cmd = self.cmd.decode()
                            client.send(b"5 / 5")
                            remove = None
                        except :
                            pass
        for client in clients_connectes:
            try :
                client.close()
            except :
                pass
        self.connect.close()
    def quit(self):
        """ Stop Loop of thread"""
        self.exit = False
        self.connect.close()
        self._Thread__stop()