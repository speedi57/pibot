import random
import time
import os
if os.name != 'nt' :
    import smbus
from threading import Thread
class Mesure(Thread):
    """Mesure Value of I2c"""
    def __init__(self):
        Thread.__init__(self)
        self.message = {'Avance': 1, 'Droite': '2','Gauche':3,'Recule':4,'Stop':0}
        self.mesure= True
        self.data = None
        self.r = [0,0,0,0,0]
        if os.name != 'nt' :
            self.bus = smbus.SMBus(1)
            self.address = 0x04      #7 bit address (will be left shifted to add the read write bit)

    def run(self):
        """treading loop"""
        while self.mesure:
            try :
                if os.name != 'nt' :
                    for i in range(5):
                        self.r[i] = self.readNumber()
                        time.sleep(0.1)
                    if self.data != None :
                        print 'data send'
                        print self.data
                        self.writeNumber(self.message[self.data])
                        time.sleep(0.1)
                        self.data = None
                else:
                    for i in range(5):
                        self.r[i] = random.choice(range(100))
            except Exception as e:
                print e

    def quit(self):
        """ Stop Loop of thread"""
        self.mesure = False

    def Send(self, data):
        """Save Data """
        if data > -1 :
            self.data = data
    def writeNumber(self,value):
        self.bus.write_byte(self.address, value)
        # bus.write_byte_data(address, 0, value)
        return -1

    def readNumber(self):
        self.number = self.bus.read_byte(self.address)
        # number = bus.read_byte_data(address, 1)
