#!/usr/bin/env python
# -*- coding:utf-8 -*-

__version__ = "0.1"
__all__ = ["PiBot"]
__author__ = "Benji" 

import sys, pygame
from pygame.locals import *
import time
import subprocess
import os
from module import Tactile,Mesure, Com
def quit():
    mesure.quit()
    serveur.quit()
    serveur.join()
    mesure.join()
    pygame.quit()
    sys.exit(0) 
if os.name != 'nt' :
    os.environ["SDL_FBDEV"] = "/dev/fb1"
    os.environ["SDL_MOUSEDEV"] = "/dev/input/touchscreen"
    os.environ["SDL_MOUSEDRV"] = "TSLIB"
size = width, height = 320, 240
pygame.init()
screen = pygame.display.set_mode(size)
mesure = Mesure()
mesure.start()
serveur = Com()
serveur.start()
Draw = Tactile(screen,pygame)
Draw.view()
cmd = None
# Main loop
while 1:   
    Draw.SetText("distance : "+str(mesure.r[1]),190,20)
    if serveur.cmd != "":
        Draw.SetText("cmd : "+str(serveur.cmd),190,70)
        mesure.Send(serveur.cmd)
    if cmd == -1 :
        quit()
    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONDOWN:
            pos = (pygame.mouse.get_pos() [0], pygame.mouse.get_pos() [1])
            cmd = Draw.on_click(pos)
            mesure.Send(cmd)
#ensure there is always a safe way to end the program if the touch screen fails   
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                quit()
    pygame.display.update()
